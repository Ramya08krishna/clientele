export const environment = {
    "envId": "fb98401d-a624-a9b1-464c-0d7a78b4f727",
    "name": "prod",
    "properties": {
        "production": true,
        "baseUrl": "http://localhost:3000",
        "tenantName": "jatahworx",
        "appName": "Clientele",
        "namespace": "com.neutrinos.jatahworx.Clientele",
        "isNotificationEnabled": false,
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "messagingSenderId": "FIREBASE_SENDER_ID"
    }
}