export const environment = {
    "envId": "50cd3061-c439-b5db-8a93-2ae719b013e6",
    "name": "dev",
    "properties": {
        "production": false,
        "baseUrl": "http://localhost:3000",
        "tenantName": "jatahworx",
        "appName": "Clientele",
        "namespace": "com.neutrinos.jatahworx.Clientele",
        "isNotificationEnabled": false,
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "messagingSenderId": "FIREBASE_SENDER_ID"
    }
}