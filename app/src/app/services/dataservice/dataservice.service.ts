/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import  {Http,Response,Headers,RequestOptions} from '@angular/http';
import { environment } from '../../../environments/environment';
@Injectable()
export class dataserviceService {
baseUrl :any;
      username : string = "bhive-art-proxyuser";
  password : string = "artproxy@13579!#%&()";
    headers = new Headers();
  options = new RequestOptions({ headers: this.headers });
  
	constructor(private  http:Http)
  {
    //this.baseUrl = environment.properties.baseUrl;
    this.baseUrl = "http://localhost:3000"
      this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', 'Basic '+ btoa(this.username + ":" +this. password));
  }
   
  getAll()
  {
    console.log(this.baseUrl);
    console.log("Insideservoce");
    return this.http.get(this.baseUrl+`/bhive-art/jatahworx/datamodel/jatahworx-rt/clientele/occupation`,{headers : this.headers})
      .map((response:Response) => response.json());
  }
  
  getAllbanknames()
  {
    return this.http.get(this.baseUrl+`/bhive-art/jatahworx/datamodel/jatahworx-rt/clientele/banknames`,{headers : this.headers})
      .map((response:Response) => response.json());
  }
   getAllbranchdetails()
  {
    return this.http.get(this.baseUrl+`/bhive-art/jatahworx/datamodel/jatahworx-rt/clientele/branchdetails`,{headers : this.headers})
      .map((response:Response) => response.json());
  }
  
  sendData(data :Object)
  {
    console.log(data);
    console.log(this.baseUrl);
    return this.http.put(this.baseUrl+`/bhive-art/jatahworx/datamodel/jatahworx-rt/clientele/memberdetails`,data, this.options)
                             .map((response: Response) => response.json());
  }
  
  getclientdetails()
  {
    console.log(this.baseUrl);
    return this.http.get(this.baseUrl+`/bhive-art/jatahworx/datamodel/jatahworx-rt/clientele/memberdetails`,{headers : this.headers})
      .map((response:Response) => response.json());
    
  }
}
