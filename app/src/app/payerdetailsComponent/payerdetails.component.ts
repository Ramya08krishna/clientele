/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { dataserviceService } from '../services/dataservice/dataservice.service';
import {Router,ActivatedRoute} from '@angular/router';
import {MdSnackBar} from '@angular/material';
/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-payerdetails',
    templateUrl: './payerdetails.template.html'
})

export class payerdetailsComponent  implements OnInit {
    banknames:any;
  branchnames:any;
  flag =false;
  bankflag = false;
  client:any;
 relation:any;
  ispolicy:any;
 payerdetails : any = {};

 retainflag=false;
  triggerflag = false;
  constructor(private dataservice:dataserviceService,private router:Router,private route:ActivatedRoute,private snackBar: MdSnackBar)
  {
    
  }
    ngOnInit()
  {
     
	this.dataservice.getAllbanknames().subscribe((res) =>{
      this.banknames = res;
      console.log(this.banknames);
    });
      this.dataservice.getAllbranchdetails().subscribe((res) => {
        this.branchnames = res;
        console.log(this.branchnames);
      });
    
    if(typeof(Storage) !== undefined)
    {
      if(JSON.parse(sessionStorage.getItem('data')) != null)
         {
     this.client = JSON.parse(sessionStorage.getItem("data"));
     console.log(this.client);
      console.log(this.client.name);
         }
    }
    
     if(typeof(Storage) !== undefined)
    {
       if(JSON.parse(sessionStorage.getItem('payerdetails')) != null)
         {
           this.payerdetails = JSON.parse(sessionStorage.getItem('payerdetails'));
         
    
    }
   
    }
      if(this.payerdetails.name == this.client.name)
           {
             this.triggerflag = true;
               this.payerdetails.ispolicy = true;
              this.payerdetails.relation = "self";
             console.log(this.payerdetails.relation);
             this.payerdetails.title=this.client.title;
             console.log(this.payerdetails.title);
             console.log(this.payerdetails.ispolicy);
           }
           else
           {
              this.triggerflag = false;
              this.payerdetails.ispolicy = false;
             
             console.log(this.payerdetails.ispolicy);
           }
    
    }
                                                 
   titles = [
    {value: 'Mr', viewValue: 'Mr'},
    {value: 'Mrs', viewValue: 'Mrs'}
]
    relations = [
      {value : 'self',viewValue:'self'},
    {value : 'aunt', viewValue : 'aunt'},
    {value : 'boyfriend', viewValue : 'boyfriend'}
    ]

   accounttype = [
    {value : 'savings', viewValue : 'Savings'},
    {value : 'current', viewValue : 'Current'},
     {value : 'checking', viewValue : 'Checking'},
     {value : 'electronic', viewValue : 'Electronic'}
    ]

  onClick()
  {
    this.flag = !this.flag;
  }
  
  bank()
  {
    this.bankflag = !this.bankflag;
  }
  
  checkbox(event)
  {
   
   	 console.log(event.target.checked);
    if(event.target.checked)
    {
       this.triggerflag = true;
     	this.payerdetails = this.client;      
      this.payerdetails.title = this.client.title;
      this.payerdetails.relation = "self";
      this.payerdetails.checked = true;
      console.log("Payerdetails ",this.payerdetails);
      
    }
    else
    {
      this.triggerflag = false;
       this.payerdetails = "";

    }
  }
  
  
  back()
    {
      this.router.navigate(['/additionalmember']);
    }
    
    onSubmit(data : any)
    {
      console.log(data.value);
      console.log(data.valid);
      if(data.valid)
      {
        console.log(data);
    
               sessionStorage.setItem("payerdetails",JSON.stringify(data.value));
        console.log(data.value);
        this.router.navigate(['/policypage']);
            
         
       
       
      }
      else	
      {
        this.snackBar.open('Enter all fields and continue...','close',{
          duration:1000,
        });
      }
    }

}
