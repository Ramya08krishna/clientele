/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { datamodelComponent } from '../datamodelComponent/datamodel.component';
import  {dataserviceService} from '../services/dataservice/dataservice.service'
import {NgForm} from '@angular/forms';
import {Router,ActivatedRoute} from '@angular/router';
import {MdSnackBar} from '@angular/material';

/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-policypage',
    templateUrl: './policypage.template.html'
})

export class policypageComponent extends datamodelComponent implements OnInit {
  flag = false;
content:any;
client:any;
name;
  surname;
  constructor(private dataservice:dataserviceService,private router:Router,private route:ActivatedRoute,private snackBar:MdSnackBar)
  {
    super();
  }
  
    ngOnInit()
  {
  		  if(typeof(Storage) !== undefined)
    {
      if(JSON.parse(sessionStorage.getItem('data')) != null)
         {
     this.client = JSON.parse(sessionStorage.getItem("data"));
     console.log(this.client);
         }
    }
    
    }
                                                          
                                 
   checkbox(event)
    {
       if(!event.target.checked)
       {
         this.flag = false;
          
       }
      else
      {
        this.flag = true;
      }
    }
  
  back()
  {
    this.router.navigate(['/payer']);
  }
  checkflag : any;
  submit()
  {
    if(this.flag)
    {
       this.content  ={
            "memberdetails" : this.client=JSON.parse(sessionStorage.getItem("data")),
        	"spousedetails" : this.spouse=JSON.parse(sessionStorage.getItem("spousedetails")),
        	"childrendetails" : this.children=JSON.parse(sessionStorage.getItem("childdetails")),
        	"extendedmemberdetails" : this.extended=JSON.parse(sessionStorage.getItem("memberdetails")),
        	"payerdetails" : JSON.parse(sessionStorage.getItem("payerdetails"))
      }
      
  for(var i=0;i<this.children.length;i++)
{
	for(var j=1;j<this.children.length && i != j ; j++)
   {
      if(this.children[i].idnumber == this.children[j].idnumber)
      {
      	        console.log(this.children[i].idnumber);

        console.log("match");
        this.snackBar.open('duplicate entry of idnumber  in children details please check...','close',{ 
       duration:3000});
        this.checkflag = true;
      }
     
     
    }
}
      
 for(var i=0;i<this.extended.length;i++)
{
	for(var j=1;j<this.extended.length && i != j ; j++)
   {
      if(this.extended[i].idnumber == this.extended[j].idnumber)
      {
      	        console.log(this.extended[i].idnumber);

        console.log("match");
           this.snackBar.open('duplicate entry of idnumber  in member details please check...','close',{ 
       duration:3000});
        this.checkflag = true;
      }
     
     
    }
}

  if(!this.checkflag)
  {
         this.dataservice.sendData(this.content).subscribe((res)=> { console.log(res) });
          console.log(this.content);
    this.router.navigate(['/member']);
     this.snackBar.open('Your policy is been added successfully!!','close',{ 
       duration:3000});
    sessionStorage.clear();
  }
 
  
  
    

      
    }
    else
    {
      this.snackBar.open('Please accept the terms and condition','close',{ 
       duration:3000});
       
      
    }
  }

}
