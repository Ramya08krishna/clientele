  /*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, Input } from '@angular/core';
import { dataserviceService } from '../services/dataservice/dataservice.service';
import { Details } from '../models/Details.model';
import { title } from '../models/title.model';
import { Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { MdSnackBar } from '@angular/material';

/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
  selector: 'bh-member',
  templateUrl: './member.template.html'
})

export class memberComponent extends Details implements OnInit {
  occupations:any;
  value: any;
  submitted = false;
  user: any;
  client: any = { };
  
  constructor(private dataservice: dataserviceService, private router: Router, private snackBar: MdSnackBar,private detail : Details) {
super();



    if (typeof (Storage) != "undefined") {
      if (JSON.parse(sessionStorage.getItem("data"))) {
        var client1 = JSON.parse(sessionStorage.getItem("data"));

        console.log(client1);
        this.client = client1;
        console.log(this.client);
      }
    }

  }

  ngOnInit() {


    this.dataservice.getAll().subscribe((res) => {
      this.occupations = res;
    });
  }

 

titles= [
    { value: 'Mr', viewValue: 'Mr' },
    { value: 'Mrs', viewValue: 'Mrs' }
  ]

  incomes = [
    { value: 'select', viewValue: 'select' },
    { value: 'noincome', viewValue: 'No Income' },
    { value: 'upto3k', viewValue: 'Up to 3000' }
  ]

  educations = [
    { value: 'metric', viewValue: 'Metric' },
    { value: 'postmetric', viewValue: 'Post metric' },
    { value: 'grad', viewValue: 'Graduate' }
  ]

 
  onlyNumber(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  onSubmit(user) {
    console.log(user.value);
    console.log(user.valid);
    this.submitted = true;
    if (user.valid) {
      if (typeof (Storage) != undefined) {
        sessionStorage.setItem("data", JSON.stringify(user.value));
        this.snackBar.open('insured details saved', 'close', {
          duration: 3000
        });
        this.router.navigate(['/additionalmember']);
      }
    }
    else {
      this.snackBar.open('Enter all spouse fields', 'close', {
        duration: 3000
      });
    }
  }


}

