/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit,AfterContentInit,DoCheck } from '@angular/core'
import { Router,ActivatedRoute,Params } from '@angular/router';
import { dataserviceService } from '../services/dataservice/dataservice.service';
import {MdSnackBar} from '@angular/material';
/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */
/*ddcds*/
@Component({
    selector: 'bh-additionalmember',
    templateUrl: './additionalmember.template.html'
})

export class additionalmemberComponent  implements OnInit {
 childArray : Array<any> =[];
  memberArray : Array<any>=[];
  spcnt:any;
     flag = false;
  detailflag = false;
  extendflag = false;

  
 /* spouse:any = {
        title:"" ,
            name : "",
            surname : "",
            gender : "",
            idnumber:"",
            dob : ""      
  };*/


	spouse:any = {};
  /*children:Array<any> = 
      [{
          name : "",
          surname : "",
          gender : "",
          idnumber : "",
          dob : ""
    }];*/
  children:Array<any> = [];
 /*extended:Array<any> = 
      [{
           title : "",
          name : "",
          surname : "",
          gender : "",
          idnumber : "",
          relation : "",
          dob : ""
    }];*/
  extended:Array<any> = [];
   client:any;
  data :any;
  gender:any;
  
  
  
  constructor(private route: ActivatedRoute, private  router : Router,private snackBar: MdSnackBar,private dataservice:dataserviceService)
  { 
   
   		
       
    
  }
    ngOnInit() {
      console.log(this.spouse.title);
      
       if(typeof(Storage) != undefined)
      {
			if(JSON.parse(sessionStorage.getItem('data')))
            {
               this.client = JSON.parse(sessionStorage.getItem('data'));
               console.log(this.client);
            }
       }
      
       if(typeof(Storage) != undefined)
       {
         if(JSON.parse(sessionStorage.getItem('spousedetails')) != null)
         {
           
      this.spouse = JSON.parse(sessionStorage.getItem('spousedetails'));
               console.log(this.spouse);
                  
         }
       }
       if(typeof(Storage) != undefined)
       {
            if(JSON.parse(sessionStorage.getItem('childdetails')) != null)
         {
        this.children = JSON.parse(sessionStorage.getItem('childdetails'));
               console.log(this.children);
         }
       }
        if(typeof(Storage) != undefined)
       {
            if(JSON.parse(sessionStorage.getItem('memberdetails')) != null)
         {
          this.extended= JSON.parse(sessionStorage.getItem('memberdetails'));
               console.log(this.extended);
         }
       }
        this.spcnt = 0;
		 if(this.client.title == "Mr")
              {
              this.spouse.title = "Mrs";
              }
           else
           {
               this.spouse.title = "Mr";
           }
    }	
  
onlyNumber(event)
  {
     return (event.charCode == 8 || event.charCode == 0) ? null: event.charCode >= 48 && event.charCode <= 57; 
  }
           
        titles = [
    {value: 'Mr', viewValue: 'Mr'},
    {value: 'Mrs', viewValue: 'Mrs'}
]
 genders = [
    {value: 'male', viewValue: 'male'},
    {value: 'female', viewValue: 'female'}
]
  
  relations = [
    {value : 'aunt', viewValue : 'aunt'},
    {value : 'boyfriend', viewValue : 'boyfriend'}
   ]
      triggerflag = false;
  
  onClick()
  {
    this.flag = !this.flag;
    if(this.flag)
    {
      console.log("Inside click"+this.flag);
              if(this.client.title == "Mr")
                  {
                    //enter value not not viewvalue content
                     this.spouse.title = "Mrs";     
                    this.spouse.gender = "female";
                    this.triggerflag = true;
                  }
                else
                {
                  this.spouse.title = "Mr";     
                  this.spouse.gender = "male";
                  this.triggerflag = true;
                }
    }
    else
    {
      console.log("inside click"+this.flag);
       if(typeof(Storage) != undefined)
       {
         if(JSON.parse(sessionStorage.getItem('spousedetails')) != null)
         {
      	 sessionStorage.removeItem("spousedetails");
           this.spouse ="";
         }
       }
    }
  }
  savespouse(spouseForm : any)
  {
    if(this.spcnt  >= 1)
    {
      this.snackBar.open('Only one spouse details is to be entered','close',{duration : 2000});
    }
    else
    {
               this.onClick;

    console.log(spouseForm.value);
    console.log(spouseForm.valid);
       if(spouseForm.valid)
                     {
                                  
                       if(this.client.idnumber == spouseForm.value.idnumber)
                       {
                          this.snackBar.open('id number exists in spouse please check...','close',{duration:3000});
                       }
                       else
                       {
                         this.spcnt += 1;
                                   sessionStorage.setItem("spousedetails",JSON.stringify(spouseForm.value));
                                  if(typeof(Storage) !== "undefined")
                                  {

                                    if(JSON.parse(sessionStorage.getItem("spousedetails")))
                                    {
                                       spouseForm = JSON.parse(sessionStorage.getItem("spousedetails"));
                                      console.log(spouseForm);

                                    }
                                               this.snackBar.open('saved spouse details','close',{duration:3000});
                                       }
                       }
                     }
        else
        {
           this.snackBar.open('enter valid spouse details','close',{ 
           duration:3000});
        }
    }
  }
  
  addChildren()
{
	this.detailflag = !this.detailflag;
  console.log(this.detailflag);
}
  
  save(child : any)
  {
     console.log(child.value);
    console.log(child.valid);
    if(this.children.length >= 3)
    {
       alert("only 3 childrens");
      this.detailflag = !this.detailflag;
  

    }
    else
    {
             if(child.valid)
              {
                
                console.log(this.spouse.idnumber);
					if((this.client.idnumber ==  child.value.idnumber) ||(this.spouse.idnumber == child.value.idnumber))
                    {
                      this.snackBar.open('duplicate idnumber in children','close',{ 
             duration:3000});
                    }
                else
                {
                          this.children.push(child.value);
                         console.log(this.children);
                        
                          sessionStorage.setItem("childdetails",JSON.stringify(this.children));
							
                         
                               this.snackBar.open('saved child','close',{ 
             duration:3000});
                }
                               
                        

                                        
                                                  
                              
                }              
       

              else
              {
                 this.snackBar.open('enter valid child details','close',{ 
                 duration:3000});
              }
    }
        
     
    }

  cancel()
  {
       this.detailflag = false;
  }
  
 removechild(index : any)
 {
   console.log("inside remove"+index);
   this.children.splice(index,1);

  	sessionStorage.setItem('childdetails',JSON.stringify(this.children));
     console.log("id number is"+index);
   
   
  }
  addextendemembers()
  {
    this.extendflag = !this.extendflag;
  console.log(this.extendflag);
  
  }
  
  savemembers(member : any)
  {
   	console.log(member.value);
  
     if(this.extended.length>=8)
    {
       alert("only 8 members");
     
    
    }
    else
    {
         if(member.valid)
                       {
                         	if((this.client.idnumber ==  member.value.idnumber) ||(this.spouse.idnumber == member.value.idnumber))
                    {
                      this.snackBar.open('duplicate idnumber in member','close',{ 
             duration:3000});
                    }
                else
                {
                         if((member.value.title == "Mr" && member.value.gender == "male") || (member.value.title == "Mrs" && member.value.gender == "female"))
                         {
                                                     this.extended.push(member.value);
                         console.log(this.extended);
                        
                          sessionStorage.setItem("memberdetails",JSON.stringify(this.extended));
							
                         
                               this.snackBar.open('saved members','close',{ 
             duration:3000});
                         }
                              else
                              {
                                  this.snackBar.open('please check the gender....','close',{duration:3000});
                              }
                    
                }

                         
                          
                       }
          else
          {
             this.snackBar.open('enter valid member details','close',{ 
             duration:3000});
          }
    }        
    }
    
  
  
   cancelmember()
  {
       this.extendflag = false;
  }
  
  removemember(index : any)
  {
     console.log("inside remove member"+index);
   this.extended.splice(index,1);
  	sessionStorage.setItem('memberdetails',JSON.stringify(this.extended));
    
     console.log("id number is"+index);
  
  }
  
    back()
    {
       var client = JSON.parse(sessionStorage.getItem("data"));
      console.log(client);
      if(typeof(Storage) !== undefined)
      {
      localStorage.setItem("data",JSON.stringify(client));
      
      }
      this.router.navigate(['/member']);
    }
    
    onsubmit()
    {
      
      this.router.navigate(['/payer']);
       
       
    }

}



