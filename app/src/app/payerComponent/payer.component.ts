/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { datamodelComponent } from '../datamodelComponent/datamodel.component';
import { dataserviceService } from '../services/dataservice/dataservice.service';
import {Router,ActivatedRoute} from '@angular/router';
/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-payer',
    templateUrl: './payer.template.html'
})

export class payerComponent extends datamodelComponent implements OnInit {
  banknames:any;
  branchnames:any;
  constructor(private dataservice:dataserviceService,private router:Router,private route:ActivatedRoute)
  {
    super();
  }
    ngOnInit() {
				 
	this.dataservice.getAllbanknames().subscribe((res) =>{
      this.banknames = res;
      console.log(this.banknames);
    });
      this.dataservice.getAllbranchdetails().subscribe((res) => {
        this.branchnames = res;
        console.log(this.branchnames);
      });
    }
                                                 
   titles:any = [
    {value: 'mr-0', viewValue: 'Mr'},
    {value: 'mrs-1', viewValue: 'Mrs'}
]
    relations = [
    {value : 'aunt-0', viewValue : 'aunt'},
    {value : 'boyfriend-1', viewValue : 'boyfriend'}
    ]

   accounttype = [
    {value : 'savings', viewValue : 'Savings'},
    {value : 'current', viewValue : 'Current'},
     {value : 'checking', viewValue : 'Checking'},
     {value : 'electronic', viewValue : 'Electronic'}
    ]

  checkbox(event)
  {
    if(event.target.checked)
    {
      this.dataservice.getclientdetails().subscribe((res) => {
        this.client = res;
        var i = this.client.length-1;
        this.payerdetails.title = this.client[i].title;
        this.payerdetails.name = this.client[i].name;
             this.payerdetails.surname = this.client[i].surname;
             this.payerdetails.idnumber = this.client[i].idnumber;
       this.payerdetails.mobilenumber = this.client[i].mobilenumber;
      this.payerdetails.worknumber = this.client[i].worknumber;
      this.payerdetails.homenumber = this.client[i].homenumber;
      this.payerdetails.email = this.client[i].email;
      this.payerdetails.postaladdress = this.client[i].postaladdress;
      this.payerdetails.suburn = this.client[i].suburn;
       this.payerdetails.city = this.client[i].towncity; 
       this.payerdetails.postalcode = this.client[i].postalcode;
        this.payerdetails.value=this.client[i].value;
        console.log(this.payerdetails);
      });
      alert("checked");
    }
    else
    {
      alert("unchecked");
      this.payerdetails.title = "";
       this.payerdetails.name = "";
             this.payerdetails.surname ="";
             this.payerdetails.idnumber = "";
       this.payerdetails.mobilenumber = "";
      this.payerdetails.worknumber = "";
      this.payerdetails.homenumber = "";
      this.payerdetails.email = "";
      this.payerdetails.postaladdress ="";
      this.payerdetails.suburn = "";
       this.payerdetails.city = ""; 
       this.payerdetails.postalcode = "";
      
    }
  }
  
   back()
    {
      this.router.navigate(['/homepage/additionalmember']);
    }
    
    onsubmit()
    {
      this.router.navigate(['/homepage/policypage',this.payerdetails.email,this.payerdetails.value]);
    }
}
