/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'

/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-datamodel',
    templateUrl: './datamodel.template.html'
})

export class datamodelComponent  {
  client : any;
  spouse : any;
  children : any;
  extended : any;
  payerdetails : any;
  bankdetails : any;
  constructor()
  {
          this.client = 
        {
            title : "",
                name : "",
                surname : "",
                idnumber: "",
                mobilenumber : "",
               worknumber : "",
              homenumber :"",
              email :"" ,
              postaladdress : "" ,
              suburn: "",
              towncity: "",
            postalcode : "",
            monthlyincome : "",
            occupation : "",
            education : "",
            value:""
        }
    
    this.spouse = 
      {
      		clientidnumber: "",
      		value:"",
            title: "",
            name : "",
            surname : "",
            gender : "",
            idnumber:"",
            dob : ""            
   	 }
    
    this.children = 
      {
          clientidnumber : "",
          value : "",
          name : "",
          surname : "",
          gender : "",
          idnumber : "",
          dob : ""
    }
    
    this.extended = 
      {
         clientidnumber : "",
          value : "",
           title : "",
          name : "",
          surname : "",
          gender : "",
          idnumber : "",
          relation : "",
          dob : ""
    }
  
  
  	this.payerdetails = 
      {
      		clientidnumber : "",
          value : "",
      		title : "",
             name : "",
             surname : "",
      idnumber : "",
      mobilenumber : "",
      worknumber : "",
      homenumber : "",
      email : "",
      relation : "",
      postaladdress :"",
      suburn : "",
      city: "",
      postalcode : "",
     
      
    }

 this.bankdetails = 
   {
   	clientidnumber : "",
          value : "",
     bankname : "",
     branchname : "",
     accounttype : "",
   accountnumber : ""
   
 }
}
}
