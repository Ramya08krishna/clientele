import { PageNotFoundComponent } from '../not-found.component';
import { LayoutComponent } from '../layout/layout.component';
import { TestComponent } from '../test/test.component';
import { NotificationService } from '../service/notification.service';
import { LocalStorageService } from '../service/local-storage.service';
import { ImgSrcDirective } from '../directives/imgSrc.directive';
import { BAuthGuard } from '../service/bAuthGuard.service';
import { BAppService } from '../service/bApp.service';
import { BLocalStorageService } from '../service/bLocalStorage.service';
import { BSessionStorage } from '../service/bSessionStorage.service';
import { BLoginService } from '../service/bLogin.service';
import { HttpModule, Http, XHRBackend, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { BHttp } from '../service/bHTTP';
import { BHTTPLoader } from '../service/bHTTPLoader';
import { PubSubService } from '../service/pubSub.service';
import { AlertComponent } from '../alertComponent/alert.component'; 
import { BDataSourceService } from '../service/bDataSource.service';
import { additionalmemberComponent } from '../additionalmemberComponent/additionalmember.component';
import { homepageComponent } from '../homepageComponent/homepage.component';
import { memberComponent } from '../memberComponent/member.component';
import { payerComponent } from '../payerComponent/payer.component';
import { payerdetailsComponent } from '../payerdetailsComponent/payerdetails.component';
import { policyComponent } from '../policyComponent/policy.component';
import { policypageComponent } from '../policypageComponent/policypage.component';
import { datamodelComponent } from '../datamodelComponent/datamodel.component';
import { dataserviceService } from '../services/dataservice/dataservice.service';
  //CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-authguardService
import { authguardService } from '../services/AuthGuard/authguard.service';
//CORE_REFERENCE_IMPORT-authguardComponent
import { authserviceService } from '../services/authservice/authservice.service';



export function httpFactory(backend: XHRBackend, defaultOptions: RequestOptions, bHTTPLoader: BHTTPLoader) {
  return new BHttp(backend, defaultOptions, bHTTPLoader);
}


/**
*bootstrap for @NgModule
*/
export const appBootstrap: any = [
  LayoutComponent,
];

/**
*Entry Components for @NgModule
*/
export const appEntryComponents: any = [
  AlertComponent
];

/**
*declarations for @NgModule
*/
export const appDeclarations = [
  ImgSrcDirective,
  LayoutComponent,
  TestComponent,
  AlertComponent,
  additionalmemberComponent,
  datamodelComponent,
  homepageComponent,
  memberComponent,
  payerComponent,
  payerdetailsComponent,
  policyComponent,
  policypageComponent,
  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-auth.guardComponent
  
  PageNotFoundComponent
];

/**
* provider for @NgModuke
*/
export const appProviders = [
  {
    provide: Http,
    useFactory: httpFactory,
    deps: [XHRBackend, RequestOptions, BHTTPLoader]
  },
  NotificationService,
  BAuthGuard,
  //CORE_REFERENCE_PUSH_TO_PRO_ARRAY
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-authguardService
authguardService,
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-authserviceService
authserviceService,
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-dataserviceService
dataserviceService,
  LocalStorageService,
  PubSubService,
  BLoginService,
  BSessionStorage,
  BLocalStorageService,
  BAppService,
  BHTTPLoader,
  BDataSourceService,
  dataserviceService,
  
  

];

/**
* Routes available for bApp
*/

// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_START
export const appRoutes = [{path: 'member', component: memberComponent},{path: 'additionalmember', component: additionalmemberComponent, canActivate: [authguardService]},{path: 'payer', component: payerdetailsComponent, canActivate: [authguardService]},{path: 'policypage', component: policypageComponent, canActivate: [authguardService]},{path: 'additionalmember/:idnumber/:title/:value', component: additionalmemberComponent},{path: 'policypage/:email/:name/:surname/:value', component: policypageComponent},{path: 'payer/:value', component: payerdetailsComponent},{path: '', redirectTo: 'member', pathMatch: 'full'},{path: '**', component: PageNotFoundComponent}]
// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_END

